package animal; /* Equivalente a namespace de C++ y PHP */

import javax.swing.JOptionPane; /* Equivale a include (C/C++/C#) o use (PHP) */

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 *
**/

public class Aplicacion 
{
    
    private static void imprimeEspecie(Animal bicho)
    {
        System.out.println( "Nombre común: "+ bicho.nombre );
        System.out.println( "Especie: "+ bicho.obtenEsp().toUpperCase() + " (" + bicho.obtenAli() + ")" );
        System.out.println( "Esperanza de vida: " + bicho.obtenLong() +" años" );
        System.out.println( "Peso máximo: hasta " + (bicho.obtenPeso()/1000) +" Kg." );
        System.out.println();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {

        Animal lobo, coyote, correcaminos;
 
        
        lobo = new Animal( "Lobo", "Canis lupus", "carnivora", 16, 100000 );
        lobo.ataque = "muerde";
        
        coyote = new Animal(lobo);
        coyote.nombre = "Coyote";
        coyote.fijaEspyAli( "Canis latrans", coyote.obtenAli() );
        coyote.fijaLongyPeso( 8, 16000 );
        
        correcaminos = new Animal("Correcaminos", "Geococcyx californianus", "carnivora", 8, 600 );
        lobo.ataque = "picotea";
      
        Animal garrapata = new Animal( "garrapata", "Culicidae", "hematofaga", 0, 1 );
        lobo.ataque = "pica";
        
  
        
        imprimeEspecie(lobo);
        coyote.imprime();
        Aplicacion.imprimeEspecie(correcaminos);
        Animal.imprimeEspecie(garrapata);

    }
        
}
