package animal;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 *
**/

public class Animal {
    
    public String nombre;
    public String especie;
    public String alimentacion;
    public float longevidad;
    public double maxpeso;
    public String ataque;

    
    public Animal( String nombreComun, String especie, String alimentacion, float longevidad, double maxpeso )
    {
        this.nombre = nombreComun;
        this.especie = especie;
        this.alimentacion = alimentacion;
        this.longevidad = longevidad;
        this.maxpeso = maxpeso;
        this.ataque = "ataca";
    }
    public Animal( Animal bicho )
    {
        this.nombre = bicho.nombre;
        this.ataque = bicho.ataque;
        this.especie = bicho.especie;
        this.alimentacion = bicho.alimentacion;
        this.longevidad = bicho.longevidad;
        this.maxpeso = bicho.maxpeso;
    }
    
    
    
    public void fijaEspyAli( String especie, String alimentacion )
    {
        this.especie = especie;
        this.alimentacion = alimentacion;
    }
    
    public String obtenEsp( )
    {
        return this.especie;
    }
    
    public String obtenAli( )
    {
        return this.alimentacion;
    }
    
    public void fijaLongyPeso( float longevidad, double maxpeso )
    {
        this.longevidad = longevidad;
        this.maxpeso = maxpeso;
    }

    public float obtenLong( ) 
    {
        return this.longevidad;
    }
    
    public double obtenPeso( )
    {
        return this.maxpeso;
    }

    public void imprime()
    {
        System.out.println( "* Nombre común: "+ this.nombre );
        System.out.println( "* Especie: "+ this.obtenEsp().toUpperCase() + " (" + this.obtenAli() + ")" );
        System.out.println( "* Esperanza de vida: " + this.obtenLong() +" años" );
        System.out.println( "* Peso máximo: hasta " + (this.obtenPeso()/1000) +" Kg." );
        System.out.println( "*" );
    }
    
    public static void imprimeEspecie(Animal bicho)
    {
        System.out.println( "(( Nombre común: "+ bicho.nombre );
        System.out.println( "(( Especie: "+ bicho.obtenEsp().toUpperCase() + " (" + bicho.obtenAli() + ")" );
        System.out.println( "(( Esperanza de vida: " + bicho.obtenLong() +" años" );
        System.out.println( "(( Peso máximo: hasta " + (bicho.obtenPeso()/1000) +" Kg." );
        System.out.println( "((" );
    }
}
